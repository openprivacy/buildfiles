#!/bin/sh

WINDOWS="https://dist.torproject.org/torbrowser/13.0.5/tor-expert-bundle-windows-x86_64-13.0.5.tar.gz"
MAC="https://www.torproject.org/dist/torbrowser/13.0.5/tor-browser-macos-13.0.5.dmg"
LINUX="https://www.torproject.org/dist/torbrowser/13.0.5/tor-browser-linux-x86_64-13.0.5.tar.xz"
ARM7="https://archive.torproject.org/tor-package-archive/torbrowser/13.0.4/tor-expert-bundle-android-armv7-13.0.4.tar.gz"
ARM64="https://archive.torproject.org/tor-package-archive/torbrowser/13.0.4/tor-expert-bundle-android-aarch64-13.0.4.tar.gz"
TORVERSION="0.4.8.9"

echo "Fetching..."

if [ ! -f $(basename $WINDOWS) ]; then
  wget $WINDOWS
fi

if [ ! -f $(basename $MAC) ]; then
  wget $MAC
fi

if [ ! -f $(basename $LINUX) ]; then
  wget $LINUX
fi

if [ ! -f $(basename $ARM7) ]; then
  wget $ARM7
fi

if [ ! -f $(basename $ARM64) ]; then
  wget $ARM64
fi

echo "Extracting..."

7z x $(basename $MAC)
tar -xf $(basename $LINUX)
tar -xf $(basename $WINDOWS)


echo "Packaging..."

cd Tor\ Browser/Tor\ Browser.app/Contents/MacOS/
rm -r Tor/PluggableTransports
chmod a+x Tor/tor
chmod a+x Tor/libevent-2.1.7.dylib
tar -czf tor-$TORVERSION-macos.tar.gz Tor
cd  ../../../..
mv Tor\ Browser/Tor\ Browser.app/Contents/MacOS/tor-$TORVERSION-macos.tar.gz .

cd tor-browser/Browser/TorBrowser
tar -czf tor-$TORVERSION-linux-x86_64.tar.gz Tor
cd ../../..
mv  tor-browser/Browser/TorBrowser/tor-$TORVERSION-linux-x86_64.tar.gz .

rm -r Tor
rm -r Data
mv tor Tor
mv data Data
zip tor-$TORVERSION-win64.zip -r Tor Data

tar -xzf $(basename $ARM7)
mv tor/libTor.so tor-$TORVERSION-android-arm7

tar -xzf $(basename $ARM64)
mv tor/libTor.so tor-$TORVERSION-android-arm64
