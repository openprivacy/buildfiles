#!/bin/sh

cd zlib/
./configure --prefix=$PWD/install
make
make install
cd ..

cd openssl/
./config --prefix=$PWD/install no-shared no-dso
make
make install
cd ..

cd libevent/
./configure --disable-shared --prefix=$PWD/install
make 
make install
cd ..

cd tor/
./configure --prefix=$PWD/install --enable-static-tor --disable-asciidoc --disable-systemd --enable-static-zlib --with-zlib-dir=./../zlib/install/ --enable-static-libevent --with-libevent-dir=./../libevent/install --enable-static-openssl --with-openssl-dir=./../openssl/install
make
make install
cd ..
