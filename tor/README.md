# Build processes

## Android

We are now using The Guardian Project's [tor-android](https://github.com/guardianproject/tor-android) to build Android tor libs directly. The results we want aren't the `.aar` or `.jar` in `output` but the direct libraries in `tor-android-binary/src/main/libs`.

## Linux and Android

We use [Briar's Tor Reproducer](https://code.briarproject.org/briar/tor-reproducer) to build our tor linux and android binaries. 

```
git clone https://code.briarproject.org/briar/tor-reproducer.git
```

Either edit the tor-versions.json or copy in the one from this directory

```
docker build --tag tor-reproducer:0.4.4.9 .
docker run tor-reproducer:0.4.4.9 ./build-tor.py 0.4.4.9
docker ps -a
docker cp 2798b62ed756:/opt/tor-reproducer/tor_arm_pie.zip tor-0.4.4.9-arm_pie.zip
docker cp 2798b62ed756:/opt/tor-reproducer/tor_arm64_pie.zip tor-0.4.4.9-arm64_pie.zip
docker cp 2798b62ed756:/opt/tor-reproducer/tor_linux-x86_64.zip tor-0.4.4.9-linux-x86_64.zip
docker cp 2798b62ed756:/opt/tor-reproducer/tor_linux-aarch64.zip tor-0.4.4.9-linux-aarch64.zip
```

Where 937d81a46c33 is the container ID from `docker ps -a`.

Cleanup with

```
docker container prune
docker rmi tor-reproducer:0.4.4.9
```

## Windows

For Window we are bundling binaries from the [Tor Windows Expert Bundle](https://www.torproject.org/download/tor/).

## Mac

For Mac we are bundling the binaries from [Tor Browser](https://www.torproject.org/download/). `harvet/harvest.sh` can be used to fetch, extract and package them.

## Historical

Previously we used some scripts that have been moved to `oldscripts`. They did not generate correct android binaries.
