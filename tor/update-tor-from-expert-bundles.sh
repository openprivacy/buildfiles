#!/usr/bin/env python3

import re
import os
import urllib.request

download_url="https://www.torproject.org/download/tor/"

f = urllib.request.urlopen(download_url)
download_contents = f.read().decode("utf-8")

p_download_link = re.compile("<a class=\"downloadLink\" href=\"(https:\/\/archive.torproject.org\/tor-package-archive\/torbrowser\/[0-9]+\.[0-9]+\.[0-9]+\/tor-expert-.*tar.gz)\">[0-9]+.[0-9]+.[0-9]+ \(tor ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\)")

p_arch = re.compile("tor-expert-bundle-([a-zA-Z0-9_]*)-([a-zA-Z0-9_]*)-([0-9]+\.[0-9]+\.[0-9]+)")

for line in p_download_link.findall(download_contents):
	#print(line)
	tversion = line[1]
	osarch = p_arch.search(line[0])
	_os = osarch[1]
	arch = osarch[2]
	version = osarch[3]
	#print(_os + " " + arch + " " + version)
	download = False
	if _os == "windows":
		if arch == "x86_64":
			download = True
			#print("Windows zip manually harvest")
	if _os == "macos":
		#download = True
		print("MacOS needs manual harvest of universal binaries from Tor Browser dmg with 7z")
	if _os == "android":
		if arch == "armv7" or arch == "aarch64":
			download = True
	if _os == "linux":
		if arch == "x86_64":
			download = True

	if download:
		fname = "tor-" + tversion + "-" + _os + "-" + arch + ".tar.gz"
		print("downloading " + line[0] + "...")
		os.system("curl " + line[0] + "  --output " + fname)
		if _os == "linux":
			print("pruning linux tar")
			dirname = "tor-" + tversion + "-" + _os + "-" + arch
			os.system("mkdir " + dirname)
			os.system("tar -xzf " + fname + " -C " + dirname + "/ " )
			os.system("rm -r " + dirname + "/data")
			os.system("rm -r " + dirname + "/debug")
			os.system("rm -r " + dirname + "/tor/pluggable_transports")
			# until about 0.4.8.9 it was packaged as Tor, all of Cwtch's 
			# package, run and deploy code expects a Tor director
			os.system("mv " + dirname + "/tor " + dirname + "/Tor")
			os.system("rm " + dirname + "/Tor/libstdc++.so*")
			os.system("rm " + fname)
			os.system("tar -czf " + fname + " -C " + dirname + " Tor")
		if _os == "windows":
			print("repackaging and pruning windows zip")
			dirname = "tor-" + tversion + "-" + _os + "-" + arch
			os.system("mkdir " + dirname)
			os.system("tar -xzf " + fname + " -C " + dirname + "/ " )
			os.system("rm -r " + dirname + "/data")
			os.system("rm -r " + dirname + "/debug")
			os.system("rm -r " + dirname + "/tor/pluggable_transports")
			# until about 0.4.8.9 it was packaged as Tor, all of Cwtch's 
			# package, run and deploy code expects a Tor director
			os.system("mv " + dirname + "/tor " + dirname + "/Tor")
			zipname = "tor-" + tversion + "-" + _os + "-" + arch + ".zip"
			os.system("cd " + dirname +"; zip " + zipname + " Tor/ Tor/tor.exe")
			os.system("mv " + dirname + "/" + zipname + " " + zipname)
		if _os == "android":
			print("extracting android .so")
			soname = "tor-" + tversion + "-" + _os + "-" + arch
			if arch == "aarch64":
				soname = "tor-" + tversion + "-" + _os + "-arm64"
			os.system("tar -xzf " + fname + " tor/libTor.so --strip-components 1")
			os.system("mv libTor.so " + soname)
